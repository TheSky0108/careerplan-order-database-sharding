package com.migrate.module;

import cn.hutool.core.date.DateTime;
import com.migrate.module.domain.RangeScroll;
import com.migrate.module.migrate.ScrollProcessor;
import com.migrate.module.service.MigrateService;
import com.migrate.module.task.CountCacheTask;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.rocketmq.client.exception.MQClientException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest(classes = MigrateModuleApplication.class)
@RunWith(SpringRunner.class)
public class MigrateModuleApplicationTests {

    @Resource
    private ScrollProcessor scrollProcessor;

    @Resource
    private MigrateService migrateService;

    @Resource
    private CountCacheTask countCacheTask;

    @Test
    public void scroll() {
        RangeScroll rangeScroll = new RangeScroll();
        rangeScroll.setEndTime(new DateTime());
        rangeScroll.setStartTime(DateUtils.addDays(new Date(), -100));
        rangeScroll.setTableName("order_info");
        rangeScroll.setStartScrollId("0");
        rangeScroll.setPageSize(200);
        scrollProcessor.scroll(rangeScroll);
    }

    @Test
    public void countCacheTask() {
        countCacheTask.init();
    }


    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String date = "2021-12-02 21:01:18.0";

        try {
            System.out.println(format.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
