package com.migrate.module.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 迁移表VO
 *
 * @author zhonghuashishan
 */
@Data
public class EtlProgressVO implements Serializable {

    private static final long serialVersionUID = -2757138049339990855L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 逻辑模型名（逻辑表）
     */
    private String logicModel;
    /**
     * 迁移批次
     */
    private String ticket;
    /**
     * 当前所属批次阶段号
     */
    private Integer curTicketStage;
    /**
     * 进度类型(0滚动抽数,1核对抽数)
     */
    private Integer progressType;
    /**
     * 迁移状态
     */
    private Integer status;
    /**
     * 已完成记录数
     */
    private Integer finishRecord;
    /**
     * 异常记录数
     */
    private Integer errorRecord;
    /**
     * 记录上一次滚动最后记录的滚动字段值
     */
    private String scrollId;
    /**
     * 开始滚动时间
     */
    private String scrollTime;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 修改时间
     */
    private String updateTime;

}
