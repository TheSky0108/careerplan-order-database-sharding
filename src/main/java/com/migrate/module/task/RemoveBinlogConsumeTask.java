package com.migrate.module.task;

import com.migrate.module.mapper.migrate.EtlBinlogConsumeRecordMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务移除掉已提交的增量同步消息
 *
 * @author zhonghuashishan
 */
@Slf4j
@Component
public class RemoveBinlogConsumeTask {

    /**
     * binlog消息同步消费记录表Mapper
     */
    @Autowired
    private EtlBinlogConsumeRecordMapper consumeRecordMapper;

    @Scheduled(cron = "0 0 0 1/1 * ? ")
    public void removeBinlogConsumeRecordTask() {
        consumeRecordMapper.deleteCommittedConsumedRecords();
    }
}
