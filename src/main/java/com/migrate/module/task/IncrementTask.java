package com.migrate.module.task;

import com.migrate.module.migrate.LocalQueue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 负责定时增量数据写入落地
 *
 * @author zhonghuashishan
 */
@Slf4j
@Component
public class IncrementTask {

    /**
     * 负责增量数据的写入动作
     */
    @Scheduled(fixedDelay = 15000)
    void IncrementTask() {
        // 获取阻塞队列的方法
        LocalQueue localQueue = LocalQueue.getInstance();
        // 验证读队列的数据已被处理完毕
        if (!localQueue.getIsRead()) {
            log.info("增量数据执行写入");
            // 执行数据写入
            localQueue.doCommit();
        }
    }
}
