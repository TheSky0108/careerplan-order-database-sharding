package com.migrate.module;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.shardingsphere.shardingjdbc.spring.boot.SpringBootConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动类
 *
 * @author zhonghuashishan
 */
@EnableScheduling
//注释掉shardingsphere的配置类，shardingsphere使用java config的方式进行配置
@SpringBootApplication(exclude = SpringBootConfiguration.class)
public class MigrateModuleApplication {
    public static void main(String[] args) throws MQClientException {
        SpringApplication.run(MigrateModuleApplication.class, args);
    }
}
