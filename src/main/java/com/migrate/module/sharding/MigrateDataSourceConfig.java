package com.migrate.module.sharding;

import lombok.Data;

import java.util.List;

/**
 * 数据库配置
 *
 * @author zhonghuashishan
 */
@Data
public class MigrateDataSourceConfig {
    /**
     * 数据源
     */
    private List<DataSourceConfig> dataSources;
    /**
     * 分片策略
     */
    private List<TableRuleConfig> tableRules;
    /**
     * 是否显示 shardingsphere sql执行日志
     */
    private Boolean sqlshow;
    /**
     * 每个逻辑库中表的数量
     */
    private int tableNum;
}
