package com.migrate.module.sharding;

import lombok.Data;

/**
 * 数据库配置
 *
 * @author zhonghuashishan
 */
@Data
public class DataSourceConfig {
    /**
     * driverClassName
     */
    private String driverClassName;
    /**
     * url
     */
    private String url;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
}