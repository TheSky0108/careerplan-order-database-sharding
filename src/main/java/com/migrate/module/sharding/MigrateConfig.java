package com.migrate.module.sharding;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 数据库配置
 *
 * @author zhonghuashishan
 */
@Data
@Component
@ConfigurationProperties(prefix = "migrate")
@PropertySource("classpath:migrate.properties")
public class MigrateConfig {

    /**
     * 数据迁移应用自己的数据源
     */
    private DataSourceConfig migrateDatasource;
    /**
     * 源数据源配置
     */
    private MigrateDataSourceConfig originDatasource;
    /**
     * 目标数据源配置
     */
    private MigrateDataSourceConfig targetDatasource;
}