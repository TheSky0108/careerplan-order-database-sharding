package com.migrate.module.config;

import com.migrate.module.sharding.MigrateConfig;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 原库数据源配置
 *
 * @author zhonghuashishan
 */
@Configuration
@MapperScan(basePackages = "com.migrate.module.mapper.db1", sqlSessionTemplateRef = "SqlSessionTemplate01")
public class DataSource1Config extends AbstractDataSourceConfig {
    @Autowired
    private MigrateConfig migrateConfig;

    @Bean(name = "DataSource01")
    @Primary
    public DataSource dataSource() throws SQLException {
        return buildDataSource(migrateConfig.getOriginDatasource());
    }

    @Bean(name = "SqlSessionFactory01")
    @Primary
    public SqlSessionFactory sqlSessionFactory(@Qualifier("DataSource01") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setTypeAliasesPackage("com.migrate.module.domain");
        bean.setConfigLocation(new ClassPathResource("mybatis/mybatis-config.xml"));
        String locationPattern = CollectionUtils.isNotEmpty(migrateConfig.getOriginDatasource().getTableRules()) ?
                "classpath*:mybatis/mapper/db1/sharding/*.xml" : "classpath*:mybatis/mapper/db1/monomer/*.xml";
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(locationPattern));
        return bean.getObject();
    }

    @Bean(name = "TransactionManager01")
    @Primary
    public DataSourceTransactionManager transactionManager(@Qualifier("DataSource01") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "SqlSessionTemplate01")
    @Primary
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("SqlSessionFactory01") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
