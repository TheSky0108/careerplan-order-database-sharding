package com.migrate.module.exception;

/**
 * 任务异常类
 *
 * @author zhonghuashishan
 */
public class BusinessException extends RuntimeException {
    private Integer code;

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
        this.code = 2;
    }

    public BusinessException(String message, Integer code) {
        super(message);
        this.code = code;
    }
}
