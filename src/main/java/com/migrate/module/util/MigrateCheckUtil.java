package com.migrate.module.util;

import com.migrate.module.migrate.MergeConfig;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 数据迁移比对工具类
 *
 * @author zhonghuashishan
 */
@Slf4j
public abstract class MigrateCheckUtil {
    /**
     * 比对时间戳
     */
    private static final String TIME_STAMP = "updateTime";

    /**
     * 防止抽象工具类通过被子类继承的方式实例化
     */
    private MigrateCheckUtil() {

    }

    /**
     * 用于比对数据的值是否一致
     *
     * @param binLogMap 监听的binLog数据对象
     * @param targetMap 新库对应的数据对象
     * @return 比对结果
     */
    public static Boolean comparison(Map<String, Object> binLogMap, Map<String, Object> targetMap, String tableName) {
        // 判断 数据是否一致
        String binLogHashCode = getReconcileHashCode(binLogMap, tableName);

        String targetHashCode = getReconcileHashCode(targetMap, tableName);
        // 如果数据不一致，校验更新时间差异，以时间最新的为准(直接校验时间，可能数据是一致的也更新)
        if (!binLogHashCode.equals(targetHashCode)) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                Date binLogUpdateTime = format.parse(binLogMap.get(TIME_STAMP) + "");

                Date targetUpdateTime = format.parse(targetMap.get(TIME_STAMP) + "");
                // 如果同步的时间大于新库的时间，则代表需要更新
                if (binLogUpdateTime.compareTo(targetUpdateTime) > 0) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("数据比对出错", e);
                return false;
            }
        }
        return false;
    }

    /**
     * 拼凑数据
     *
     * @param instanceCountMap binLog数据对象
     * @return 数据拼接结果
     */
    private static String getReconcileHashCode(Map<String, Object> instanceCountMap, String tableName) {
        StringBuilder reconcileHashCode = new StringBuilder(1024);
        List<String> fieldList = MergeConfig.getFiledKey(tableName);
        for (String field : fieldList) {
            reconcileHashCode.append(field).append(instanceCountMap.get(field));
        }
        return reconcileHashCode.toString();
    }
}
