package com.migrate.module.util.mock;

import java.util.concurrent.CountDownLatch;

/**
 * 多线程执行数据插入
 */
public class BatchInsert {
    public static void main(String[] args) {
        long startTimes = System.currentTimeMillis();
        int threadCount = 100;
        int total = 1000;
        int every = total / threadCount;
        final CountDownLatch latch = new CountDownLatch(threadCount);
        for (int i = 0; i < threadCount; i++) {
            new Thread(new Worker(latch, i * every, (i + 1) * every)).start();
        }
        try {
            latch.await();
            long endTimes = System.currentTimeMillis();
            System.out.println("所有线程执行完毕:" + (endTimes - startTimes));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}

class Worker implements Runnable {

    int start = 0;
    int end = 0;
    CountDownLatch latch;

    public Worker(CountDownLatch latch, int start, int end) {
        this.start = start;
        this.end = end;
        this.latch = latch;
    }

    @Override
    public void run() {
        for (int i = start; i < end; i++) {
            System.out.println("线程" + Thread.currentThread().getName() + "正在执行。。");
            InsertDataUtils insertDateUtils1 = new InsertDataUtils();
            insertDateUtils1.insertBigData3();
        }
        latch.countDown();
    }

}