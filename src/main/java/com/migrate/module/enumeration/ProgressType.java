package com.migrate.module.enumeration;

/**
 * 滚动类型枚举值
 *
 * @author zhonghuashishan
 */
public enum ProgressType {
    /**
     * 滚动抽数
     */
    RANGE_SCROLL("滚动抽数", 0),
    /**
     * 核对抽数
     */
    CHECK_DATA("核对抽数", 1);
    /**
     * 枚举显示名称
     */
    private final String name;
    /**
     * 枚举的值
     */
    private final Integer value;

    ProgressType(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    /**
     * 取得枚举类型的值
     *
     * @return 枚举的值
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 取得枚举类型的名称
     *
     * @return 枚举显示名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 根据枚举类型的值取得枚举类型
     *
     * @param typeValue 枚举类型的值
     * @return 枚举类型
     */
    public static ProgressType findByValue(String typeValue) {
        for (ProgressType type : values()) {
            if (type.getValue().equals(typeValue)) {
                return type;
            }
        }
        return null;
    }

    /**
     * 根据枚举类型的值取得枚举类型的名称
     *
     * @param typeValue 枚举类型的值
     * @return 枚举显示名称
     */
    public static String getNameByValue(String typeValue) {
        for (ProgressType type : values()) {
            if (type.getValue().equals(typeValue)) {
                return type.getName();
            }
        }
        return null;
    }
}
