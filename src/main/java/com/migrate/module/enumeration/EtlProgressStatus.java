package com.migrate.module.enumeration;

/**
 * 同步状态
 *
 * @author zhonghuashishan
 */
public enum EtlProgressStatus {


    INIT("同步中", 1),
    SUCCESS("同步完成", 2),
    FAIL("同步失败", 3),
    CHECK_SUCCESS("核对校验完成", 4);


    /**
     * 枚举显示名称
     */
    private final String name;
    /**
     * 枚举的值
     */
    private final Integer value;

    EtlProgressStatus(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    /**
     * 取得枚举类型的值
     *
     * @return 枚举的值
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 取得枚举类型的名称
     *
     * @return 枚举显示名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 根据枚举类型的值取得枚举类型
     *
     * @param typeValue 枚举类型的值
     * @return 枚举类型
     */
    public static EtlProgressStatus findByValue(String typeValue) {
        for (EtlProgressStatus type : values()) {
            if (type.getValue().equals(typeValue)) {
                return type;
            }
        }
        return null;
    }

    /**
     * 根据枚举类型的值取得枚举类型的名称
     *
     * @param typeValue 枚举类型的值
     * @return 枚举显示名称
     */
    public static String getNameByValue(String typeValue) {
        for (EtlProgressStatus type : values()) {
            if (type.getValue().equals(typeValue)) {
                return type.getName();
            }
        }
        return null;
    }
}
