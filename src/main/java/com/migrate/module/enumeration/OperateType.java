
package com.migrate.module.enumeration;


/**
 * 操作类型，增量同步或者全量同步
 *
 * @author zhonghuashishan
 */
public enum OperateType {
    /**
     * 增量
     */
    ADD("增量", 1),
    /**
     * 全量
     */
    ALL("全量", 2);

    /**
     * 枚举显示名称
     */
    private final String name;
    /**
     * 枚举的值
     */
    private final int value;

    OperateType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    /**
     * 取得枚举类型的值
     *
     * @return 枚举的值
     */
    public int getValue() {
        return this.value;
    }

    /**
     * 取得枚举类型的名称
     *
     * @return 枚举显示名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 根据枚举类型的值取得枚举类型
     *
     * @param typeValue 枚举类型的值
     * @return 枚举类型
     */
    public static OperateType findByValue(int typeValue) {
        for (OperateType type : values()) {
            if (type.getValue() == (typeValue)) {
                return type;
            }
        }
        return null;
    }

    /**
     * 根据枚举类型的值取得枚举类型的名称
     *
     * @param typeValue 枚举类型的值
     * @return 枚举显示名称
     */
    public static String getNameByValue(int typeValue) {
        for (OperateType type : values()) {
            if (type.getValue() == typeValue) {
                return type.getName();
            }
        }
        return null;
    }
}
