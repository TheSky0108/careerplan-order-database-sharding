package com.migrate.module.enumeration;

/**
 * DB渠道
 *
 * @author zhonghuashishan
 */
public enum DBChannel {

    /**
     * 渠道一
     */
    CHANNEL_1("历史数据库", "1"),
    /**
     * 渠道二
     */
    CHANNEL_2("新的数据库", "2");

    /**
     * 枚举显示名称
     */
    private final String name;
    /**
     * 枚举的值
     */
    private final String value;

    DBChannel(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * 取得枚举类型的值
     *
     * @return 枚举的值
     */
    public String getValue() {
        return this.value;
    }

    /**
     * 取得枚举类型的名称
     *
     * @return 枚举显示名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 根据枚举类型的值取得枚举类型
     *
     * @param typeValue 枚举类型的值
     * @return 枚举类型
     */
    public static DBChannel findByValue(String typeValue) {
        for (DBChannel type : values()) {
            if (type.getValue().equals(typeValue)) {
                return type;
            }
        }
        return null;
    }

    /**
     * 根据枚举类型的值取得枚举类型的名称
     *
     * @param typeValue 枚举类型的值
     * @return 枚举显示名称
     */
    public static String getNameByValue(String typeValue) {
        for (DBChannel type : values()) {
            if (type.getValue().equals(typeValue)) {
                return type.getName();
            }
        }
        return null;
    }
}
