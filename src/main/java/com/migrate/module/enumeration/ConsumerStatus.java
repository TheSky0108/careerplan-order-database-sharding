
package com.migrate.module.enumeration;

/**
 * 操作结果枚举值
 *
 * @author zhonghuashishan
 */
public enum ConsumerStatus {
    /**
     * 未消费：0
     */
    NOT_CONSUME("未消费", 0),
    /**
     * 消费成功：1
     */
    CONSUME_SUCCESS("消费成功", 1),
    /**
     * 已提交：2
     */
    COMMITTED("已提交", 2);
    /**
     * 名称
     */
    private final String name;
    /**
     * 值
     */
    private final Integer value;

    ConsumerStatus(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    /**
     * 取得枚举类型的值
     *
     * @return 枚举类型的值
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 取得枚举类型的名称
     *
     * @return 枚举类型的名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 根据枚举类型的值取得枚举类型
     *
     * @param typeValue 枚举值
     * @return 枚举类型
     */
    public static ConsumerStatus findByValue(Integer typeValue) {
        for (ConsumerStatus type : values()) {
            if (type.getValue().equals(typeValue)) {
                return type;
            }
        }
        return null;
    }

    /**
     * 根据枚举类型的值取得枚举类型的名称
     *
     * @param typeValue 枚举值
     * @return 枚举类型的名称
     */
    public static String getNameByValue(Integer typeValue) {
        for (ConsumerStatus type : values()) {
            if (type.getValue().equals(typeValue)) {
                return type.getName();
            }
        }
        return null;
    }
}
