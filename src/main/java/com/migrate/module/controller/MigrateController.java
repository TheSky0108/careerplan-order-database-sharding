package com.migrate.module.controller;

import com.migrate.module.domain.EtlProgress;
import com.migrate.module.domain.RangeScroll;
import com.migrate.module.enumeration.OperateResult;
import com.migrate.module.migrate.ScrollProcessor;
import com.migrate.module.service.MigrateService;
import com.migrate.module.util.DateUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据同步迁移配置入口
 *
 * @author zhonghuashishan
 */
@RestController
@RequestMapping("/migrate")
public class MigrateController {
    @Resource
    private MigrateService migrateService;

    @Resource
    private ScrollProcessor scrollProcessor;

    /**
     * 取得迁移进度信息
     *
     * @param queryCondition 查询条件
     * @return
     */
    @RequestMapping(value = "/getEtlProgresses", method = RequestMethod.POST)
    public Map<String, Object> getEtlProgresses(@RequestBody EtlProgress queryCondition) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("resultCode", OperateResult.SUCCESS.getValue());
        resultMap.put("resultMsg", OperateResult.SUCCESS.getName());
        List<EtlProgress> resultList = migrateService.getEtlProgresses(queryCondition);
        resultMap.put("resultList", resultList);
        return resultMap;
    }

    /**
     * 补偿失败的范围滚动拉取数据
     *
     * @param id 同步记录ID
     */
    @RequestMapping(value = "/reSync", method = RequestMethod.POST)
    public Map<String, Object> reSync(Long id) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("resultCode", OperateResult.SUCCESS.getValue());
        resultMap.put("resultMsg", OperateResult.SUCCESS.getName());
        migrateService.compensateRangeScroll(id);
        return resultMap;
    }

    /**
     * 新增全量同步  将前端传过来的世界格式化
     *
     * @param rangeScroll 全量同步条件
     * @return 保存结果
     */
    @RequestMapping(value = "/addScroll", method = RequestMethod.POST)
    public Map<String, Object> addScroll(@RequestBody RangeScroll rangeScroll) {
        rangeScroll.setStartTime(DateUtils.getStartTimeOfDate(rangeScroll.getStartTime()));
        rangeScroll.setEndTime(DateUtils.getDayEndTime(rangeScroll.getEndTime()));
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("resultCode", OperateResult.SUCCESS.getValue());
        resultMap.put("resultMsg", OperateResult.SUCCESS.getName());
        scrollProcessor.scroll(rangeScroll);
        return resultMap;
    }
}
