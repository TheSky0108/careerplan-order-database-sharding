package com.migrate.module.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单表
 *
 * @author zhonghuashishan
 */
@Data
public class Order {

    private Long id;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 订单金额
     */
    private BigDecimal orderAmount;

    /**
     * 商户ID
     */
    private Long merchantId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 运费
     */
    private BigDecimal orderFreight;

    /**
     * 订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单
     */
    private Long orderStatus;

    /**
     * 交易时间
     */
    private Date transTime;

    /**
     * 支付状态,1待支付,2支付成功,3支付失败
     */
    private Long payStatus;

    /**
     * 交易完成时间
     */
    private Date rechargeTime;

    /**
     * 实际支付金额
     */
    private BigDecimal payAmount;

    /**
     * 支付优惠金额
     */
    private BigDecimal payDiscountAmount;

    /**
     * 收货地址
     */
    private Long addressId;

    /**
     * 配送方式 1=自提 2=配送
     */
    private Long deliveryType;

    /**
     * 配送状态，0 配送中，2已送达，3待收货，4已送达
     */
    private Long deliveryStatus;

    /**
     * 配送预计送达时间
     */
    private Date deliveryExpectTime;

    /**
     * 配送送达时间
     */
    private Date deliveryCompleteTime;

    /**
     * 配送运费
     */
    private BigDecimal deliveryAmount;

    /**
     * 优惠券id
     */
    private Long couponId;

    /**
     * 订单取消时间
     */
    private Date cancelTime;

    /**
     * 订单确认时间
     */
    private Date confirmTime;

    /**
     * 订单备注留言
     */
    private String remark;

    /**
     * 创建用户
     */
    private Long createUser;

    /**
     * 更新用户
     */
    private Long updateUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 逻辑删除标记
     */
    private Byte deleteFlag;


}