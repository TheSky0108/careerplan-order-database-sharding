package com.migrate.module.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 数据抽取模型
 *
 * @author zhonghuashishan
 */
@Data
public class RangeScroll implements Serializable {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 抽数的开始时间
     */
    private Date startTime;
    /**
     * 抽数的截止时间
     */
    private Date endTime;
    /**
     * 抽数起始查询字段值
     */
    private String startScrollId;
    /**
     * 当前所属批次阶段号
     */
    private Integer curTicketStage;
    /**
     * 迁移批次
     */
    private String ticket;
    /**
     * 抽数指定的表
     */
    private String tableName;
    /**
     * 每页捞取数量
     */
    private Integer pageSize;
    /**
     * 是否重试
     */
    private Boolean retryFlag = false;
    /**
     * 已同步次数
     */
    private Integer retryTimes;
}
