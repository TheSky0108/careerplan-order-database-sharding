package com.migrate.module.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * binlog消息同步消费记录表
 *
 * @author zhonghuashishan
 */
@Data
public class EtlBinlogConsumeRecord implements Serializable {
    private static final long serialVersionUID = -228992538822874060L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 消息队列id（即：queueId）
     */
    private Integer queueId;
    /**
     * 消息偏移量（唯一定位该消息在队列中的位置）
     */
    private Long offset;
    /**
     * 消息所属主题
     */
    private String topic;
    /**
     * 消息所在broker名称
     */
    private String brokerName;
    /**
     * 消费状态，0 未消费 1 消费成功 2 已提交
     *
     * @see com.migrate.module.enumeration.ConsumerStatus
     */
    private Integer consumeStatus;
    /**
     * 记录创建时间
     */
    private Date createTime;
    /**
     * 记录更新时间
     */
    private Date updateTime;
}
