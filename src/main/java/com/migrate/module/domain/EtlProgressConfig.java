package com.migrate.module.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 迁移 配置表
 *
 * @author zhonghuashishan
 */
@Data
public class EtlProgressConfig implements Serializable {
    /**
     * 主键
     */
    private Long id;
    /**
     * 逻辑模型名(逻辑表或模型名称)
     */
    private String logicModel;
    /**
     * 迁移批次模型字段名称
     */
    private String recordKey;
    /**
     * 迁移字段匹配类型(0唯一字段,1查询匹配字段)
     */
    private Integer recordType;
}
