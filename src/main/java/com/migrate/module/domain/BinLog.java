package com.migrate.module.domain;

import lombok.Data;

import java.util.Map;

@Data
public class BinLog {

    /**
     * binlog对应的表名
     */
    private String tableName;
    /**
     * 唯一的key
     */
    private String key;
    /**
     * 操作时间
     */
    private Long operateTime;
    /**
     * 操作类型
     */
    private String operateType;
    /**
     * 单条sql的信息
     */
    private Map<String, Object> dataMap;

}
