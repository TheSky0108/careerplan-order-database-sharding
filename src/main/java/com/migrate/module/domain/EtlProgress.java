package com.migrate.module.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 迁移表
 *
 * @author zhonghuashishan
 */
@Data
public class EtlProgress implements Serializable {

    private static final long serialVersionUID = 3381526018986361684L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 逻辑模型名（逻辑表）
     */
    private String logicModel;
    /**
     * 迁移批次
     */
    private String ticket;
    /**
     * 当前所属批次阶段号
     */
    private Integer curTicketStage;
    /**
     * 进度类型(0滚动抽数,1核对抽数)
     */
    private Integer progressType;
    /**
     * 迁移状态
     */
    private Integer status;
    /**
     * 已同步次数
     */
    private Integer retryTimes;
    /**
     * 已完成记录数
     */
    private Integer finishRecord;
    /**
     * 已完成进度
     */
    private BigDecimal ProgressScale;
    /**
     * 记录上一次滚动最后记录的滚动字段值
     */
    private String scrollId;
    /**
     * 开始滚动时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date scrollTime;
    /**
     * 滚动截止时间
     */
    private Date scrollEndTime;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;


}
