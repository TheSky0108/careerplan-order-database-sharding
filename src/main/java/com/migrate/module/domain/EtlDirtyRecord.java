package com.migrate.module.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 迁移明细表
 *
 * @author zhonghuashishan
 */
@Data
public class EtlDirtyRecord implements Serializable {

    /**
     * 主键
     */
    private Long id;
    /**
     * 逻辑模型名（逻辑表）
     */
    private String logicModel;

    /**
     * 迁移批次
     */
    private String ticket;
    /**
     * 当前所属批次阶段号
     */
    private Integer curTicketStage;
    /**
     * 字段名
     */
    private String recordKey;
    /**
     * 字段值
     */
    private String recordValue;
    /**
     * 迁移状态
     */
    private Integer status;
    /**
     * 错误消息
     */
    private String errorMsg;
    /**
     * 每页条数
     */
    private Integer syncSize;
    /**
     * 已重试次数
     */
    private Integer retryTimes;
    /**
     * 上次重试时间
     */
    private Date lastRetryTime;
    /**
     * 开始滚动时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date scrollTime;
    /**
     * 滚动截止时间
     */
    private Date scrollEndTime;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
