package com.migrate.module.constants;


import java.math.BigDecimal;
import java.util.HashMap;

/**
 * 数据常量
 *
 * @author zhonghuashishan
 */
public class Constants {

    /**
     * 滚动批次总数据量缓存 每小时更新一次
     */
    public static HashMap<String, BigDecimal> statisticalCountMap = new HashMap<>();
}
