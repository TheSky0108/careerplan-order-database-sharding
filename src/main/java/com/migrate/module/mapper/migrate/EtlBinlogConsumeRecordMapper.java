package com.migrate.module.mapper.migrate;

import com.migrate.module.domain.EtlBinlogConsumeRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * binlog消息同步消费记录表Mapper
 *
 * @author zhonghuashishan
 */
@Repository
public interface EtlBinlogConsumeRecordMapper {
    /**
     * 新增消费记录
     *
     * @param consumeRecord 消费记录
     */
    void insert(EtlBinlogConsumeRecord consumeRecord);

    /**
     * 更新消费记录状态
     *
     * @param consumeRecord 要更新的消费记录
     */
    void updateConsumeRecordStatus(EtlBinlogConsumeRecord consumeRecord);

    /**
     * 批量更新消费记录状态
     *
     * @param queueId           消息队列id
     * @param consumeStatus     消费状态
     * @param consumeRecordList 要更新的消费记录集合
     */
    void batchUpdateConsumeRecordStatus(@Param("queueId") Integer queueId, @Param("consumeStatus") Integer consumeStatus, @Param("list") List<EtlBinlogConsumeRecord> consumeRecordList);

    /**
     * 取得未消费的记录
     *
     * @return 未消费的记录
     */
    List<EtlBinlogConsumeRecord> getNotConsumedRecords();

    /**
     * 取得未提交的记录
     *
     * @return 未提交的记录
     */
    List<EtlBinlogConsumeRecord> getNotCommittedConsumedRecords();

    /**
     * 删除已提交的记录，提交过的记录就无用了可以删除了
     *
     * @return
     */
    List<EtlBinlogConsumeRecord> deleteCommittedConsumedRecords();

    /**
     * 根据队列ID的偏移量获取消息同步记录
     *
     * @param queueId 队列ID
     * @param offset  偏移量
     * @return 消息同步记录
     */
    EtlBinlogConsumeRecord getExistsRecord(@Param("queueId") Integer queueId, @Param("offset") Long offset);
}
