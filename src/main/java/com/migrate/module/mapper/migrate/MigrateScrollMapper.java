package com.migrate.module.mapper.migrate;

import com.migrate.module.domain.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 数据同步的基础mapper
 *
 * @author zhonghuashishan
 */
public interface MigrateScrollMapper {
    /**
     * 初始化写入一条同步迁移记录
     *
     * @param etlProgress
     * @return
     */
    int insertEtlProgress(EtlProgress etlProgress);

    /**
     * 更新迁移记录的数据
     *
     * @param etlProgress
     * @return
     */
    int updateEtlProgress(EtlProgress etlProgress);

    /**
     * 查询迁移的进度列表
     *
     * @param etlProgress
     * @return
     */
    List<EtlProgress> queryEtlProgressList(EtlProgress etlProgress);

    /**
     * 查询未同步完成的表，数据量总和
     *
     * @return
     */
    List<EtlProgress> queryMiddleProgressList();

    /**
     * 查询迁移的进度详情
     *
     * @param id
     * @return
     */
    EtlProgress queryEtlProgressById(Long id);

    /**
     * 查询核对校验的详情
     *
     * @param rangeScroll
     * @return
     */
    EtlProgress queryEtlProgressCheck(RangeScroll rangeScroll);

    /**
     * 查询全部的配置信息
     *
     * @return
     */
    List<EtlProgressConfig> queryEtlProgressConfigList();

    /**
     * 查询迁移明细
     *
     * @param etlDirtyRecord
     * @return
     */
    List<EtlDirtyRecord> queryDirtyRecordList(EtlDirtyRecord etlDirtyRecord);

    /**
     * 新增一条迁移进度明细记录
     *
     * @param etlDirtyRecord
     * @return
     */
    int insertEtlDirtyRecord(EtlDirtyRecord etlDirtyRecord);

    /**
     * 更新迁移进度明细记录
     *
     * @param etlDirtyRecord
     * @return
     */
    int updateEtlDirtyRecord(EtlDirtyRecord etlDirtyRecord);

    /**
     * 新增某表的一日数据统计
     *
     * @param etlStatistical
     * @return
     */
    int insertEtlStatistical(EtlStatistical etlStatistical);

    /**
     * 更新某表的一日数据统计
     *
     * @param etlStatistical
     * @return
     */
    int updateEtlStatistical(EtlStatistical etlStatistical);

    /**
     * 查询单个表，单个日期的记录
     *
     * @param etlStatistical
     * @return
     */
    EtlStatistical getEtlStatistical(EtlStatistical etlStatistical);


    /**
     * 查询单个表最大的日期统计记录
     *
     * @param etlStatistical
     * @return
     */
    EtlStatistical getMaxDateEtlStatistical(EtlStatistical etlStatistical);

    /**
     * 查询统计的记录
     *
     * @param etlStatistical
     * @return
     */
    List<EtlStatistical> queryEtlStatisticalList(EtlStatistical etlStatistical);

    /**
     * 获取数据迁移的数据大概数量
     *
     * @param etlStatistical
     * @return
     */
    BigDecimal getStatisticalCount(EtlStatistical etlStatistical);

    /**
     * 取得支持全量同步的表
     *
     * @return 支持全量同步的表
     */
    List<String> getScrollAbleTables();
}
