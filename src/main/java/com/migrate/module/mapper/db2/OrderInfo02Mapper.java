package com.migrate.module.mapper.db2;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 目标库订单表数据库操作
 *
 * @author zhonghuashishan
 */
public interface OrderInfo02Mapper {

    /**
     * 插入数据
     *
     * @param dataMap 订单数据
     * @return 状态
     */
    int insert(Map<String, Object> dataMap);

    /**
     * 批量插入数据
     *
     * @param dataMaps 订单数据集合
     * @return 状态
     */
    int insertBat(@Param("dataMaps") List<Map<String, Object>> dataMaps);

    /**
     * 更新数据
     *
     * @param dataMap 订单数据
     * @return 状态
     */
    int update(Map<String, Object> dataMap);

    /**
     * 删除数据
     *
     * @param dataMap 订单数据
     * @return 状态
     */
    int delete(Map<String, Object> dataMap);

    /**
     * 通过orderNo集合查询订单数据
     *
     * @param identifiers 订单号数据
     * @return 状态
     */
    List<Map<String, Object>> selectByIdentifiers(List<String> identifiers);

}
