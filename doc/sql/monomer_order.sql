/*
 Navicat Premium Data Transfer

 Source Server         : 订单单库
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 47.101.162.131:3306
 Source Schema         : monomer_order

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 06/01/2022 21:36:26
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for etl_binlog_consume_record
-- ----------------------------
DROP TABLE IF EXISTS `etl_binlog_consume_record`;
CREATE TABLE `etl_binlog_consume_record`
(
    `id`             bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `queue_id`       int(11) DEFAULT NULL COMMENT '消息队列id（即：queueId）',
    `offset`         bigint(20) DEFAULT NULL COMMENT '消息偏移量（唯一定位该消息在队列中的位置）',
    `topic`          varchar(500) DEFAULT NULL COMMENT '消息所属主题',
    `broker_name`    varchar(255) DEFAULT NULL COMMENT '消息所在broker名称',
    `consume_status` tinyint(1) DEFAULT NULL COMMENT '消费状态，0 未消费 1 消费成功 2 已提交',
    `create_time`    datetime     DEFAULT NULL COMMENT '记录创建时间',
    `update_time`    datetime     DEFAULT NULL COMMENT '记录更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `queue_id` (`queue_id`,`offset`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=597 DEFAULT CHARSET=utf8mb4 COMMENT='binlog消息同步消费记录表';

-- ----------------------------
-- Table structure for etl_dirty_record
-- ----------------------------
DROP TABLE IF EXISTS `etl_dirty_record`;
CREATE TABLE `etl_dirty_record`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `logic_model`      varchar(255) NOT NULL COMMENT '逻辑模型名(逻辑表或模型名称)',
    `ticket`           varchar(32)  NOT NULL COMMENT '迁移批次',
    `cur_ticket_stage` int(10) NOT NULL COMMENT '当前所属批次阶段号',
    `record_key`       varchar(60)  NOT NULL COMMENT '字段名',
    `record_value`     varchar(128)          DEFAULT NULL COMMENT '字段值',
    `status`           int(12) DEFAULT NULL COMMENT '迁移状态',
    `error_msg`        varchar(500)          DEFAULT NULL COMMENT '错误消息',
    `retry_times`      int(12) DEFAULT NULL COMMENT '已重试次数',
    `last_retry_time`  datetime              DEFAULT NULL COMMENT '上次重试时间',
    `is_deleted`       tinyint(1) DEFAULT '0' COMMENT '0:未被删除,1:已删除',
    `create_time`      datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`      datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `sync_size`        int(11) DEFAULT '0' COMMENT '每次同步数量',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2098 DEFAULT CHARSET=utf8mb4 COMMENT='迁移明细表';

-- ----------------------------
-- Table structure for etl_progress
-- ----------------------------
DROP TABLE IF EXISTS `etl_progress`;
CREATE TABLE `etl_progress`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `logic_model`      varchar(255)      DEFAULT NULL COMMENT '逻辑模型名(逻辑表或模型名称)',
    `ticket`           varchar(32)       DEFAULT NULL COMMENT '迁移批次',
    `cur_ticket_stage` int(10) DEFAULT NULL COMMENT '当前所属批次阶段号',
    `progress_type`    int(10) DEFAULT NULL COMMENT '进度类型(0滚动抽数,1核对抽数)',
    `status`           int(12) DEFAULT NULL COMMENT '迁移状态,1同步中,2同步完成,3同步失败',
    `retry_times`      int(11) DEFAULT '0' COMMENT '已同步次数',
    `finish_record`    bigint(20) DEFAULT '0' COMMENT '已完成记录数',
    `scroll_id`        varchar(100)      DEFAULT '0' COMMENT '记录上一次滚动最后记录字段值',
    `scroll_time`      datetime          DEFAULT NULL COMMENT '开始滚动时间',
    `scroll_end_time`  datetime          DEFAULT NULL COMMENT '滚动截止时间',
    `is_deleted`       tinyint(1) DEFAULT '0' COMMENT '0:未被删除,1:已删除',
    `create_time`      datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`      datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='迁移表';

-- ----------------------------
-- Table structure for etl_progress_config
-- ----------------------------
DROP TABLE IF EXISTS `etl_progress_config`;
CREATE TABLE `etl_progress_config`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `logic_model` varchar(255)      DEFAULT NULL COMMENT '逻辑模型名(逻辑表或模型名称)',
    `record_key`  varchar(32)       DEFAULT NULL COMMENT '迁移批次模型字段名称',
    `record_type` int(10) DEFAULT NULL COMMENT '迁移字段匹配类型(0唯一字段,1查询匹配字段)',
    `is_deleted`  tinyint(1) DEFAULT '0' COMMENT '0:未被删除,1:已删除',
    `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COMMENT='迁移配置表';

-- ----------------------------
-- Table structure for etl_statistical
-- ----------------------------
DROP TABLE IF EXISTS `etl_statistical`;
CREATE TABLE `etl_statistical`
(
    `id`                bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `logic_model`       varchar(255)      DEFAULT NULL COMMENT '逻辑模型名(逻辑表或模型名称)',
    `statistical_count` bigint(20) DEFAULT NULL COMMENT '统计数据量',
    `statistical_time`  int(8) DEFAULT NULL COMMENT '统计时间(按天为单位)',
    `is_deleted`        tinyint(1) DEFAULT '0' COMMENT '0:未被删除,1:已删除',
    `create_time`       datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`       datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`id`),
    KEY                 `inx_statistical_time` (`statistical_time`),
    KEY                 `inx_logic_model` (`logic_model`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8mb4 COMMENT='迁移数据统计表';

-- ----------------------------
-- Table structure for order_info
-- ----------------------------
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_order_no` (`order_no`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84656407 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_item_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail`;
CREATE TABLE `order_item_detail`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

SET
FOREIGN_KEY_CHECKS = 1;
