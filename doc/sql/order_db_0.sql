/*
 Navicat Premium Data Transfer

 Source Server         : 8库
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 139.224.82.136:3307
 Source Schema         : order_db_0

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 06/01/2022 21:42:40
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_info_sharded_by_user_id_0
-- ----------------------------
DROP TABLE IF EXISTS `order_info_sharded_by_user_id_0`;
CREATE TABLE `order_info_sharded_by_user_id_0`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    UNIQUE KEY `uinx_order_no` (`order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_update_time` (`update_time`,`order_no`) USING BTREE,
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84660617 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_info_sharded_by_user_id_1
-- ----------------------------
DROP TABLE IF EXISTS `order_info_sharded_by_user_id_1`;
CREATE TABLE `order_info_sharded_by_user_id_1`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    UNIQUE KEY `uinx_order_no` (`order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_update_time` (`update_time`,`order_no`) USING BTREE,
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84658644 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_info_sharded_by_user_id_2
-- ----------------------------
DROP TABLE IF EXISTS `order_info_sharded_by_user_id_2`;
CREATE TABLE `order_info_sharded_by_user_id_2`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    UNIQUE KEY `uinx_order_no` (`order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_update_time` (`update_time`,`order_no`) USING BTREE,
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84688779 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_info_sharded_by_user_id_3
-- ----------------------------
DROP TABLE IF EXISTS `order_info_sharded_by_user_id_3`;
CREATE TABLE `order_info_sharded_by_user_id_3`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    UNIQUE KEY `uinx_order_no` (`order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_update_time` (`update_time`,`order_no`) USING BTREE,
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84688430 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_info_sharded_by_user_id_4
-- ----------------------------
DROP TABLE IF EXISTS `order_info_sharded_by_user_id_4`;
CREATE TABLE `order_info_sharded_by_user_id_4`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    UNIQUE KEY `uinx_order_no` (`order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_update_time` (`update_time`,`order_no`) USING BTREE,
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84658528 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_info_sharded_by_user_id_5
-- ----------------------------
DROP TABLE IF EXISTS `order_info_sharded_by_user_id_5`;
CREATE TABLE `order_info_sharded_by_user_id_5`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    UNIQUE KEY `uinx_order_no` (`order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_update_time` (`update_time`,`order_no`) USING BTREE,
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84657479 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_info_sharded_by_user_id_6
-- ----------------------------
DROP TABLE IF EXISTS `order_info_sharded_by_user_id_6`;
CREATE TABLE `order_info_sharded_by_user_id_6`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    UNIQUE KEY `uinx_order_no` (`order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_update_time` (`update_time`,`order_no`) USING BTREE,
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84683127 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_info_sharded_by_user_id_7
-- ----------------------------
DROP TABLE IF EXISTS `order_info_sharded_by_user_id_7`;
CREATE TABLE `order_info_sharded_by_user_id_7`
(
    `id`                     bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`               varchar(32)   NOT NULL COMMENT '订单号',
    `order_amount`           decimal(8, 2) NOT NULL COMMENT '订单金额',
    `merchant_id`            bigint(32) NOT NULL COMMENT '商户ID',
    `user_id`                bigint(32) NOT NULL COMMENT '用户ID',
    `order_freight`          decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '运费',
    `order_status`           tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态,10待付款，20待接单，30已接单，40配送中，50已完成，55部分退款，60全部退款，70取消订单',
    `trans_time`             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '交易时间',
    `pay_status`             tinyint(3) NOT NULL DEFAULT '2' COMMENT '支付状态,1待支付,2支付成功,3支付失败',
    `recharge_time`          timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '支付完成时间',
    `pay_amount`             decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
    `pay_discount_amount`    decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '支付优惠金额',
    `address_id`             bigint(32) NOT NULL COMMENT '收货地址ID',
    `delivery_type`          tinyint(3) NOT NULL DEFAULT '2' COMMENT '配送方式，1自提。2配送',
    `delivery_status`        tinyint(3) DEFAULT '0' COMMENT '配送状态，0 配送中，2已送达，3待收货，4已送达',
    `delivery_expect_time`   timestamp NULL DEFAULT NULL COMMENT '配送预计送达时间',
    `delivery_complete_time` timestamp NULL DEFAULT NULL COMMENT '配送送达时间',
    `delivery_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '配送运费',
    `coupon_id`              bigint(32) DEFAULT NULL COMMENT '优惠券id',
    `cancel_time`            timestamp NULL DEFAULT NULL COMMENT '订单取消时间',
    `confirm_time`           timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `remark`                 varchar(512)           DEFAULT NULL COMMENT '订单备注留言',
    `create_user`            bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`            bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`            timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`            timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`            tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`, `order_no`),
    UNIQUE KEY `uinx_order_no` (`order_no`),
    KEY                      `inx_user_id` (`user_id`),
    KEY                      `inx_merchant_id_update_time` (`merchant_id`,`update_time`),
    KEY                      `inx_update_time` (`update_time`,`order_no`) USING BTREE,
    KEY                      `inx_create_time` (`create_time`,`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=84692006 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单表';

-- ----------------------------
-- Table structure for order_item_detail_sharded_by_user_id_0
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail_sharded_by_user_id_0`;
CREATE TABLE `order_item_detail_sharded_by_user_id_0`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

-- ----------------------------
-- Table structure for order_item_detail_sharded_by_user_id_1
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail_sharded_by_user_id_1`;
CREATE TABLE `order_item_detail_sharded_by_user_id_1`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

-- ----------------------------
-- Table structure for order_item_detail_sharded_by_user_id_2
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail_sharded_by_user_id_2`;
CREATE TABLE `order_item_detail_sharded_by_user_id_2`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

-- ----------------------------
-- Table structure for order_item_detail_sharded_by_user_id_3
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail_sharded_by_user_id_3`;
CREATE TABLE `order_item_detail_sharded_by_user_id_3`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

-- ----------------------------
-- Table structure for order_item_detail_sharded_by_user_id_4
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail_sharded_by_user_id_4`;
CREATE TABLE `order_item_detail_sharded_by_user_id_4`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

-- ----------------------------
-- Table structure for order_item_detail_sharded_by_user_id_5
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail_sharded_by_user_id_5`;
CREATE TABLE `order_item_detail_sharded_by_user_id_5`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

-- ----------------------------
-- Table structure for order_item_detail_sharded_by_user_id_6
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail_sharded_by_user_id_6`;
CREATE TABLE `order_item_detail_sharded_by_user_id_6`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

-- ----------------------------
-- Table structure for order_item_detail_sharded_by_user_id_7
-- ----------------------------
DROP TABLE IF EXISTS `order_item_detail_sharded_by_user_id_7`;
CREATE TABLE `order_item_detail_sharded_by_user_id_7`
(
    `id`                  bigint(32) NOT NULL AUTO_INCREMENT,
    `order_no`            varchar(32)   NOT NULL COMMENT '订单号',
    `product_id`          bigint(32) NOT NULL COMMENT '商品ID',
    `category_id`         bigint(32) NOT NULL COMMENT '商品分类ID',
    `goods_num`           int(8) NOT NULL DEFAULT '1' COMMENT '商品购买数量',
    `goods_price`         decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
    `goods_amount`        decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品总价',
    `product_name`        varchar(64)            DEFAULT NULL COMMENT '商品名',
    `discount_amount`     decimal(8, 2) NOT NULL DEFAULT '0.00' COMMENT '商品优惠金额',
    `discount_id`         bigint(32) DEFAULT NULL COMMENT '参与活动ID',
    `product_picture_url` varchar(128)           DEFAULT NULL COMMENT '商品图片',
    `create_user`         bigint(32) DEFAULT NULL COMMENT '创建用户',
    `update_user`         bigint(32) DEFAULT NULL COMMENT '更新用户',
    `create_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `delete_flag`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除标记',
    PRIMARY KEY (`id`) USING BTREE,
    KEY                   `inx_item_order_no` (`order_no`),
    KEY                   `inx_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218311238 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

SET
FOREIGN_KEY_CHECKS = 1;
